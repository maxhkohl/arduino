#pragma once
#include "Sensor.hxx"
class Thermometer :
	public Sensor
{
public:
	Thermometer();
	float GetPercentOffset();
	virtual ~Thermometer();
};

