#include "Fan.hxx"
#define BASE_MODEL "UNKNOWN"
#define BASE_STATE 1
#define BASE_ID 0
#define BASE_THERM NULL


Fan::Fan()
{
	Init(BASE_ID, BASE_MODEL, BASE_THERM);
}
Fan::Fan(int id)
{
	Init(id, BASE_MODEL, BASE_THERM);
}
Fan::Fan(int id, std::string model)
{
	Init(id, model, BASE_THERM);
}
Fan::Fan(int id, Thermometer* t){
	Init(id, BASE_MODEL, t);
}
Fan::Fan(int id, std::string model, Thermometer* t)
{
	Init(id, model, t);
}
void Fan::Init(int id, std::string model, Thermometer* t)
{
	ID = id;
	prevTime = 0;
	currentTime = 0;
	fanModel = model;
	parentTherm = t;
	//FanHelper.GetBaseState(&fanModel, &state);
	//FanHelper.GetPWMBounds(&fanModel, &minPWM, &maxPWM);
	Update();
}
int Fan::Update()
{
	SetTime();
	pct = parentTherm->GetPercentOffset();
	speedPWM = (short unsigned int)(maxPWM * pct);
	return 1; // Returned successfully
}
void Fan::SetTime()
{
	time(&currentTime);
	prevTime = currentTime;
	if (prevTime == 0 || currentTime == 0)
		deltaTime = -1; // Invalid delta time measurement since either of the two values required are undefined
	else
		deltaTime = currentTime - prevTime;
	// Export data by packaging for shipment into MongoDB
}

Fan::~Fan()
{
}
