
#pragma once
#include <time.h>

class Sensor
{
	public:
		//Base class for all hardware sensors
		Sensor();
		Sensor(int id);
		Sensor(bool s);
		Sensor(int id, bool s);
		int Update();
		~Sensor();
	private:
		void Init(int id, bool s);
		void SetTime();
		time_t prevTime;
		time_t currentTime;
		time_t deltaTime;
		bool state;
		int ID;
};

