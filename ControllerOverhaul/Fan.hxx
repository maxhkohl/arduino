#pragma once
#include <time.h>
#include <string>
#include "Sensor.hxx"
#include "Thermometer.hxx"
#include "FanHelper.hxx"

class Fan :
	public Sensor
{

	public:
		// Base class for all hardware Fans
		Fan();
		Fan(int id);
		Fan(int id, std::string model);
		Fan(int id, Thermometer* t);
		Fan(int id, std::string model, Thermometer* t);
		int Update();
		~Fan();

	private:
		bool state;
		int ID;
		time_t prevTime;
		time_t currentTime;
		time_t deltaTime;

		// Fan Specific Data Members
		std::string fanModel;
		Thermometer* parentTherm;
		float pct;
		unsigned short int maxPWM;
		unsigned short int minPWM;
		unsigned short int speedPWM;
		void Init(int id, std::string model, Thermometer* t);
		void SetTime();
};
