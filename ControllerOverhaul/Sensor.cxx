#pragma once
#include "Sensor.hxx"


Sensor::Sensor()
{
	Init(0, 0); // Default values of ID = 0, state = off
}
Sensor::Sensor(int id)
{
	Init(id, 0); // state = off
}
Sensor::Sensor(bool s)
{
	Init(0, s); // ID = 0
}
Sensor::Sensor(int id, bool s)
{
	Init(id, s);
}
int Sensor::Update()
{
	SetTime();
	return 1; // Returned successfully
}
void Sensor::Init(int id, bool s)
{
	ID = id;
	state = s;
	prevTime = 0;
	currentTime = 0;
	Update();
}
void Sensor::SetTime()
{
	time(&currentTime);
	prevTime = currentTime;
	if (prevTime == 0 || currentTime == 0)
		deltaTime = -1;
	else
		deltaTime = currentTime - prevTime;
	// Export data by packaging for shipment into MongoDB
}

Sensor::~Sensor()
{
}
