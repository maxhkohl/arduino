#pragma once
#include <string>

class FanHelper
{
public:
	FanHelper();

	void GetBaseState(std::string*, bool);
	void GetPWMBounds(std::string*, unsigned int*, unsigned int*);
	~FanHelper();
};

