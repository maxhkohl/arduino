


//  Included Libraries
  // DS18B20 Waterproof Thermometer
    // One Wire Protocol
      #include <OneWire.h>
    // Manufacturer Library
      #include <DallasTemperature.h>
      
  // DHT22 Thermometer and Humidity Sensor
    // DHT Library - This does not work with DHT11's!
      #include <DHT.h>
      #define DHTPIN 4     // what pin we're connected to

// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11 
#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

DHT dht(DHTPIN, DHTTYPE);

// Pin Definitions
  // Analog Pin 1
  const short unsigned int moistureSensorPin = 1;
  // Digital Pin 2
  const short unsigned int thermometerInputPin = 2; // DS18B20
  // Digital Pin 3
  const short unsigned int fanPWMPin = 3; // PWM pin linked to Blue wire of 4 wire fans


// Thermometer Declarations and Initializations...
  // Overall Thermometer tools and constants...
    static float averageTemp = 0;
    static float sum = 0;
    static const float maxTemp = 80.0f;
    static const float minTemp = 60.0f;
    int currentTemp = 0;
    
  // DS18B20 Declarations...
    OneWire oneWire(thermometerInputPin);
    DallasTemperature thermometer(&oneWire);
    /*
      The thermometer has a 64 bit address register which is constant. 
      There is a 64 bit address that is unique to every thermometer. 
      It allows for multiple OneWire devices (not just thermometers) 
      to communicate through a single Digital pin.
      
      See the tutorial on how to obtain these addresses:
      http://www.hacktronics.com/Tutorials/arduino-1-wire-address-finder.html
    */
       DeviceAddress insideThermometer = { 0x28, 0xFF, 0xA4, 0x7F, 0x4A, 0x04, 0x00, 0x36 };
       DeviceAddress lightThermometer = { 0x28, 0xFF, 0xC3, 0xF9, 0x4E, 0x04, 0x00, 0x38 };
       DeviceAddress soilThermometer = { 0x28, 0xFF, 0x7B, 0x7F, 0x4A, 0x04, 0x00, 0x07 };
// Fan Declarations and Initializations...
  static const int maxClock = 255;
  static const int minClock = 32;
  float fanSpeed;
  int fanSpeedI;
  int ratio = maxClock-minClock;
  int difference;

// OTHER Global Declarations and Initializations...
  static short int firstRun = 1;
  static int counter = 0;
  static const int TEN = 10;
  
  
/////////////////////////////////////////////////////////////////////////////////////////////
//  Function Definitions

void fanSetup()
{
  /*
    Initialize PWM fans to full duy cycle until 
    the first readings are taken from the thermometer.
    This allows the fans to have full power to spin-up,
    and ensures that heat is not an issue if the software
    fails for some reason after this point.
  */

  // Initialize fanPWMPin pin as an output pin
  pinMode(fanPWMPin, OUTPUT);
  // Turn PWM on with 100% duty cycle to start fan and ensure adequate cooling
  analogWrite(fanPWMPin,maxClock);
}



void thermometerSetup()
{
  /*
    Initialize the DS18B20's one wire protocol - 
    In the future there will be multiple sensors defined here.
  */

  // Start up DS18B20 library
  thermometer.begin();
  // set the resolution to 10 bit (good enough?)
  thermometer.setResolution(insideThermometer, 10);
}

void printTemperature(DeviceAddress deviceAddress)
{
  /*
    Used by DS18B20 to output temperature data to serial bus.
  */

  // Read temperature of given thermometer
  float tempC = thermometer.getTempC(deviceAddress);

  // Verify the reading...
  if (tempC == -127.00) {
    // Something is broken.
    Serial.print("Error getting temperature");
  } else { // Output temperature in units of your choice
    //Serial.print("C: ");
    //Serial.print(tempC); This is America, sadly...
    Serial.print(" F: ");
    Serial.println(DallasTemperature::toFahrenheit(tempC));
  }
}

float getTemperature(DeviceAddress deviceAddress)
{
  /*
    Returns the current temperature from a given thermometer...
  */
  
    // Ask the thermometer to initiate a temperature reading...
      thermometer.requestTemperatures();
    /* 
      The temperature reading is stored in a register within 
      the thermometer. getTempC reads that register of a given 
      device and returns it as *C.  
   
      The probe's manufacturer library includes a conversion function
      which is passed a float in *C and returns a float in *F
      I called it and give it the *C reading from getTempC, 
      reducing the number of variables declared by 2 or 3, depending 
      on how you program it.   
    */
    // Return *F
      return DallasTemperature::toFahrenheit(thermometer.getTempC(deviceAddress));
}


void setup()
{
  // Initialize Fans and set PWM to full speed until first adjustment is made..
  fanSetup();
  // Initialize Thermometers
  thermometerSetup();
  
  
  dht.begin();
  // Initialize Serial Port to 9600 baud
  // The speed is measured in bits per second, also known as
  // "baud rate". 9600 is a very commonly used baud rate,
  // and will transfer about 10 characters per second.
  Serial.begin(9600);
}


void loop()
{
  // Digital Temperature...
  /* 
    There is a static sum of all temperature readings taken, as well 
    as a static counter that is updated every time. Together this will 
    allow us to take accurate temperatures over a small amount of time 
    by averaging the readings together, preventing random spikes or 
    drops in temperature readings which could produce anomalous fan speeds.
    The higher the counter, the more accurate the reading is.
  */
//DHT
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(t) || isnan(h)) {
    Serial.println("Failed to read from DHT");
  } else {
    Serial.print("Humidity: "); 
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print("Temperature: "); 
    Serial.print(t);
    Serial.println(" *C");
  }
  //Serial.print("Getting temperatures...\n\r");
  //sum += getTemperature(insideThermometer);
  counter++; 
  //averageTemp = sum/counter;
  averageTemp = getTemperature(insideThermometer);
  Serial.print("inside temp: ");
  Serial.println(averageTemp);
  Serial.print("light temp: ");
  float t1 = getTemperature(lightThermometer);
  Serial.println(t1);
  Serial.print("soil temp: ");
  t1 = getTemperature(soilThermometer);
  Serial.println(t1);
  
  /*
    Now it is time to update the fan speed!
  
  */
  
  // Only modify every 5 iterations
  if (counter % 5 == 0)
  {
     updateFans(averageTemp);
  }

  // Moisture Sensor
  Serial.print("Moisture: ");
  Serial.println(analogRead(5));
   
  delay(500); // repeat once per second (change as you wish!)
}

void updateFans(float currentTemp)
{
  //Get rid of the summing average, take the current average temperature and average it against the next reading.
    //sum = averageTemp;
    //counter = 1;
    
    // Take a minimum speed, and add the ratio on top of that.
    //Minimum fan speed is 64/255
    //The remaining maxClock - minClock is your ratio
    difference = currentTemp-minTemp;
    if (difference <= 0)
    {
      fanSpeed = minClock/maxClock;
      fanSpeedI = minClock;
    }
    else if (difference > (maxTemp-minTemp))
    {
      fanSpeed = 1;
      fanSpeedI = maxClock; 
    }
    else
    {
      fanSpeed = difference/(maxTemp - minTemp);
      fanSpeedI = (fanSpeed*ratio);
    }
    
    // Output Fan Data...
    
      Serial.print("The current fan percentage is is: ");
      Serial.println(fanSpeed);
      Serial.print("The current fan speed is: ");
      Serial.println(fanSpeedI);
     // Update Fan Speed...
      analogWrite(fanPWMPin,fanSpeedI);
}
