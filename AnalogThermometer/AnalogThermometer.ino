//Analog Thermometer
// Pin Definitions Definitions
  // Analog Pin 0
  const short unsigned int temperaturePin = 0; // Default Thermometer

// Thermometer Declarations and Initializations...
  // Overall Thermometer tools and constants...
    static float averageTemp = 0;
    static float sum = 0;
    static const float maxTemp = 80.0f;
    static const float minTemp = 60.0f;
    int currentTemp = 0;
  
  // Analog Thermometer Tools...
    float voltage, degreesC, degreesF; // For default thermometer...
    const float voltageToTemperatureScalar = 0.004882814;


float getVoltage(int pin)
{
  /* 
    This function has one input parameter, the analog pin number
    to read. Returns a floating-point value, which is the true 
    voltage on that pin (0 to 5V).

    This equation converts the 0 to 1023 value that analogRead()
    returns, into a 0.0 to 5.0 value that is the true voltage
    being read at that pin.
  */
  
  return (analogRead(pin) * voltageToTemperatureScalar);
}

void setup()
{
   Serial.begin(9600); 
}

void loop()
{

  /*
    Analog Thermometer...
    First we'll measure the voltage at the analog pin and multiply
    it by the global voltageToTemperatureScalar that is defined above...
  */
  voltage = getVoltage(temperaturePin);
  
  // Now we'll convert the voltage to degrees Celsius.
  // This formula comes from the temperature sensor datasheet:
  degreesC = (voltage - 0.5) * 100.0;
  
  // While we're at it, let's convert degrees Celsius to Fahrenheit.
  // This is the classic C to F conversion formula:
  degreesF = degreesC * (9.0/5.0) + 32.0;
  
  /* 
    There is a static sum of all temperature readings taken, as well 
    as a static counter that is updated every time. Together this will 
    allow us to take accurate temperatures over a small amount of time 
    by averaging the readings together, preventing random spikes or 
    drops in temperature readings which could produce anomalous fan speeds.
  */
  
  //  The higher the counter, the more accurate the reading is.
  Serial.print("Avg temperature: ");
  Serial.println(averageTemp);
  
  sum += averageTemp;
  counter++; 
  averageTemp = sum/counter;
  Serial.print("Temp:");
  Serial.print(averageTemp);
  Serial.println(" F");
  delay(500); 
}
